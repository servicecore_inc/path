<?php

namespace ServiceCore\Path;

return [
    'service_manager' => [
        'factories' => [
            Context\Parse::class    => Factory\Parse::class,
            Context\Validate::class => Factory\Validate::class
        ]
    ]
];
