# Path 

The path module validates the _authenticity_ and _inheritance_ of the request's _path_.

## Terminology

This library differentiates between a _route_ and a _path_:

1. _Route_ - A route is a Zend Framework Matched Route. A route will match a route configured in the application's configuration array. A route will have route parameters.
2. _Path_ - A path is a sequence of segments with rules. The rules are applied sequentially to determine whether or not the path is valid.

## Purpose

Neither Zend Framework 2 nor ApiAgility support hierarchical routing out-of-the-box.

ApiAgility validates and authenticates the last segment in a route. However, it doesn't care about previous segments. For example, as far as the application is concerned _without this module_, `/companies/999/sites` is a perfectly valid route, even if there is no _Company 999_.

In addition, ApiAgility does not make sure child entities belong to parent entities. For example, as far as the application is concerned _without this module_, `/companies/1/sites/2` is a perfectly valid route, even if _Site 2_ does not belong to _Company 1_.

The former case makes our API _frail_, because exceptions will be thrown. The latter case makes our API _vulnerable_, because a user might be able to access things they shouldn't. 

## Methodology

This module listens to the application's `EVENT_ROUTE` event with a priority that makes sure it handles the event _after_ the route has been matched. If the route is not valid, this module will respond `404`. If the route is valid, this module will allow the request to continue.

## Configuration

This module requires _every_ route segment to define a `rules` array:

```php
return [
    'router' => [
        'routes' => [
            'foo' => [
                ...,
                // the new custom key!
                'rules' => [],
                'child_routes' => [
                    'bar' => [
                        ...,
                        // the new custom key!
                        'rules' => []
                    ]
                ]
            ]
        ]
    ]
];

```

Every route segment must define a `rules` array, but the value MAY be empty. This will undoubtedly raise some complaints, however, it ensures a developer must think about his/her segment's security, if even for a second or two.

## Rules

For a given path to be considered valid, _all rules_ for _all segments_ must be true. 

This library supports three types of rules: _newables_, _services_, and _closures_.

### Newable rules

A newable rule must be an _array_ with at least a `name` key, and it must implement the `RuleInterface`. Optionally, the rule may include `options` which are passed to the rule's constructor:

```php

'rules' => 
    ...,
    [
        'name'    => 'Path\To\Foo',
        'options' => [
            'foo' => 'bar',
            'baz' => 'qux'
        ]
    ],
    ...
]
```

#### Exists

A common use case is determining whether or not an entity in the route in authentic. The `Exists` rule makes sure an entity in the route exists in the database. It accepts two options `entity`, a fully-qualified entity class name, and `constraint` the name of the route parameter that holds the entity's id:

```php
'rules' => [
    ...,
    [
        'name'    => 'Path\To\Exists',
        'options' => [
            'entity'     => 'Path\To\Entity',
            'constraint' => 'entity_id'
        ]
    ]
]
```

#### Custom

If you're creating your own newable rule, you MAY extend the base `Rule` class. 

If not, make sure your newable rule implements the `RuleInterface` and its constructor accepts two arguments: the service manager and an optional options array.

```php
// a custom rule
class Custom implements RuleInterface
{
    public function __construct(ServiceLocatorInterface $services, array $options)
    {
        // something cool!
    }
}
```

### Service rules

A service rule is simply the _string_ name of a service in the service manager that implements the `RuleInterface`:

```php
'rules' => [
    ...,
    'SerivceName',
    ...
]
```

### Closure rules

Closures are _anonymous functions_ that accept a single `array` argument, `$parameters`, and return bool. Within the closure, `$this` is bound to the service manager:

```php
'rules' => [
    ...,
    function (array $parameters): bool {
        // return true if foo exists in the database
        return (bool) $this->get('doctrine.entitymanager.orm_default')
            ->find(Path\To\Foo::class, $parameters['foo_id']);
    },
    ...
]
```

That's about it!

## Version

### June 6, 2016

* Initial release

## Author

[Jack Clayton](mailto:andy@servicecore.com)
