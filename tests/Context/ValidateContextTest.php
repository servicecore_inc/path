<?php

namespace ServiceCore\Path\Test\Context;

use Doctrine\ORM\EntityManager;
use InvalidArgumentException;
use Laminas\ServiceManager\ServiceManager;
use PHPUnit\Framework\TestCase;
use ServiceCore\Path\Context\Validate;
use ServiceCore\Path\Data\{Path, Segment};
use ServiceCore\Path\Rule\RuleInterface;

/**
 * @group validate
 */
class ValidateContextTest extends TestCase
{
    /** @var Validate */
    private $context;

    public function __construct()
    {
        $serviceManager = new ServiceManager();
        $emBuilder      = $this->getMockBuilder(EntityManager::class);

        $emBuilder->disableOriginalConstructor();

        $serviceManager->setService(EntityManager::class, $emBuilder->getMock());

        $this->context = new Validate($serviceManager);

        parent::__construct();
    }

    /**
     * @return void
     */
    public function testValidateReturnsTrueIfRulesDoNotExist(): void
    {
        $path = new Path([new Segment('foo', [])]);

        $this->assertTrue($this->context->validate($path, []));
    }

    /**
     * @return void
     */
    public function testValidateThrowsExceptionIfRuleIsInvalid(): void
    {
        $this->expectException(InvalidArgumentException::class);

        // a rule must be an array (newable), string (service), or closure
        $rule    = 1;
        $rules   = [$rule];
        $segment = new Segment('foo', $rules);
        $path    = new Path([$segment]);

        $this->context->validate($path, []);
    }

    /**
     * @return void
     */
    public function testValidateThrowsExceptionIfNewableNameDoesNotExist(): void
    {
        $this->expectException(InvalidArgumentException::class);

        // a newable rule must have a key named "name"
        $rule    = ['bar' => 'baz'];
        $rules   = [$rule];
        $segment = new Segment('foo', $rules);
        $path    = new Path([$segment]);

        $this->context->validate($path, []);
    }

    /**
     * @return void
     */
    public function testValidateThrowsExceptionIfNewableNameIsNotString(): void
    {
        $this->expectException(InvalidArgumentException::class);

        // the "name" value must be a string
        $rule    = ['name' => 1];
        $rules   = [$rule];
        $segment = new Segment('foo', $rules);
        $path    = new Path([$segment]);

        $this->context->validate($path, []);
    }

    /**
     * @return void
     */
    public function testValidateThrowsExceptionIfNewableNameIsNotClass(): void
    {
        $this->expectException(InvalidArgumentException::class);

        // the "name" value must be a valid classname
        $rule    = ['name' => 'Path\\To\\Foo'];
        $rules   = [$rule];
        $segment = new Segment('foo', $rules);
        $path    = new Path([$segment]);

        $this->context->validate($path, []);
    }

    /**
     * @return void
     */
    public function testValidateThrowsExceptionIfNewableClassIsNotInterface(): void
    {
        $this->expectException(InvalidArgumentException::class);

        // the "name" value must implement the RuleInterface
        $rule    = ['name' => 'User\\Core\\Data\\User'];
        $rules   = [$rule];
        $segment = new Segment('foo', $rules);
        $path    = new Path([$segment]);

        $this->context->validate($path, []);
    }

    /**
     * @return void
     */
    public function testValidateThrowsExceptionIfNewableOptionsIsNotArray(): void
    {
        $this->expectException(InvalidArgumentException::class);

        // the "options" value must be an array
        $rule    = ['name' => 'Path\\Rule\\Exists', 'options' => 1];
        $rules   = [$rule];
        $segment = new Segment('foo', $rules);
        $path    = new Path([$segment]);

        $this->context->validate($path, []);
    }

    /**
     * @return void
     */
    public function testValidateThrowsExceptionIfServiceDoesNotExist(): void
    {
        $this->expectException(InvalidArgumentException::class);

        // the service name must be defined in the service manager
        $rule    = 'foo';
        $rules   = [$rule];
        $segment = new Segment('foo', $rules);
        $path    = new Path([$segment]);

        $this->context->validate($path, []);
    }

    /**
     * @return void
     */
    public function testValidateThrowsExceptionIfServiceIsNotInterface(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $this->context = new Validate(
            new ServiceManager(
                [
                    'services' => [
                        'foo' => new class {
                        }
                    ]
                ]
            )
        );

        $rule    = 'foo';
        $rules   = [$rule];
        $segment = new Segment('foo', $rules);
        $path    = new Path([$segment]);

        $this->context->validate($path, []);
    }

    /**
     * @return void
     */
    public function testValidateReturnsBoolIfServiceIsValid(): void
    {
        // create a service rule that implements the rule interface
        $service = new class() implements RuleInterface {
            public function apply(array $parameters): bool
            {
                return true;
            }
        };

        $this->context = new Validate(
            new ServiceManager(
                [
                    'services' => [
                        'foo' => $service
                    ]
                ]
            )
        );

        $rule    = 'foo';
        $rules   = [$rule];
        $segment = new Segment('foo', $rules);
        $path    = new Path([$segment]);

        $this->assertTrue($this->context->validate($path, []));
    }

    /**
     * @return void
     */
    public function testValidateReturnsBoolIfClosure(): void
    {
        // define a simple closure rule
        $rule = function (array $parameters): bool {
            return true;
        };

        $rules   = [$rule];
        $segment = new Segment('foo', $rules);
        $path    = new Path([$segment]);

        $this->assertTrue($this->context->validate($path, []));
    }

    /**
     * @return void
     */
    public function testValidateReturnsTrueIfRulesAreTrue(): void
    {
        // define three sets of rules (all of which return true)
        $rules1 = [
            function (array $parameters): bool {
                return true;
            }
        ];

        $rules2 = [
            function (array $parameters): bool {
                return true;
            }
        ];

        $rules3 = [
            function (array $parameters): bool {
                return true;
            }
        ];

        // create three segments
        $segment1 = new Segment('foo', $rules1);
        $segment2 = new Segment('bar', $rules2);
        $segment3 = new Segment('baz', $rules3);

        // create the path to validate
        $path = new Path([$segment1, $segment2, $segment3]);

        // validate the path
        $actual = $this->context->validate($path, []);

        $this->assertTrue($actual);
    }

    /**
     * @return void
     */
    public function testValidateReturnsFalseIfRuleIsFalse(): void
    {
        // define three sets of rules
        $rules1 = [
            function (array $parameters): bool {
                return true;
            }
        ];

        // the false rule!
        $rules2 = [
            function (array $parameters): bool {
                return false;
            }
        ];

        $rules3 = [
            function (array $parameters): bool {
                return true;
            }
        ];

        // create three segments
        $segment1 = new Segment('foo', $rules1);
        $segment2 = new Segment('bar', $rules2);
        $segment3 = new Segment('baz', $rules3);

        // create the path to validate
        $path = new Path([$segment1, $segment2, $segment3]);

        // validate the path
        $actual = $this->context->validate($path, []);

        $this->assertFalse($actual);
    }

    /**
     * @return void
     */
    public function testValidateSkipsSegment(): void
    {
        // define three sets of rules
        $rules1 = [
            function (array $parameters): bool {
                return false;
            }
        ];

        // the false rule!
        $rules2 = [
            function (array $parameters): bool {
                return true;
            }
        ];

        $rules3 = [
            function (array $parameters): bool {
                return true;
            }
        ];

        // create three segments
        $segment1 = new Segment('foo', $rules1);
        $segment2 = new Segment('bar', $rules2);
        $segment3 = new Segment('baz', $rules3);

        // create the path to validate
        $path = new Path([$segment1, $segment2, $segment3]);

        // validate the path
        $actual = $this->context->validate($path, ['skippedSegments' => ['foo']]);

        $this->assertTrue($actual);
    }
}
