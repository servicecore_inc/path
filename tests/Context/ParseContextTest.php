<?php

namespace ServiceCore\Path\Test\Context;

use Laminas\Router\Http\RouteMatch;
use OutOfBoundsException;
use PHPUnit\Framework\TestCase;
use ServiceCore\Path\Context\Parse;
use ServiceCore\Path\Data\{Path, Segment};
use ServiceCore\Route\Context\Explode;

/**
 * @group parse
 */
class ParseContextTest extends TestCase
{
    /**
     * @return mixed|null
     */
    public function testParseThrowsExceptionIfRouteDoesNotExist(): void
    {
        $this->expectException(OutOfBoundsException::class);

        $match = (new RouteMatch([]))->setMatchedRouteName('foo/bar/baz');
        $parse = new Parse(new Explode([]));

        $parse->parse($match);
    }

    /**
     * @return void
     */
    public function testParseReturnsPathIfRouteIsEmpty(): void
    {
        $match = (new RouteMatch([]))->setMatchedRouteName('');
        $parse = new Parse(new Explode([]));

        $this->assertEquals(new Path([]), $parse->parse($match));
    }

    /**
     * @return void
     */
    public function testParseReturnsPathIfRouteIsRoot(): void
    {
        $match = (new RouteMatch([]))->setMatchedRouteName('/');
        $parse = new Parse(new Explode([]));

        $this->assertEquals(new Path([]), $parse->parse($match));
    }

    /**
     * @return void
     */
    public function testParseReturnsPathIfRulesDoNotExist(): void
    {
        // create a custom routes array
        $routes = [
            'foo' => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/foo[/:foo_id]',
                    'constraints' => [
                        'foo_id' => '\d+'
                    ]
                ],
                'rules'   => []
            ]
        ];

        // instantiate a explode-route service with our custom routes array
        $exploder = new Explode($routes);

        // instantiate a parse-path service
        $parser = new Parse($exploder);

        // create a matched route
        $match = (new RouteMatch(['foo_id' => 1]))->setMatchedRouteName('foo');

        // expect a path with a single segment with no rules
        $expected = new Path([new Segment('foo', [])]);
        $actual   = $parser->parse($match);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return void
     */
    public function testParseReturnsPathIfRulesDoExist(): void
    {
        // create a rules array with a new-able, service, and closure rule
        $rules = [
            [
                'name'    => 'Path\To\Foo',
                'options' => [
                    'bar' => 'baz',
                    'qux' => 'quux'
                ]
            ],
            'Path\\To\\Service',
            function (array $parameters): bool {
                return true;
            }
        ];

        // create a custom routes array
        $routes = [
            'foo' => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/foo[/:foo_id]',
                    'constraints' => [
                        'foo_id' => '\d+'
                    ]
                ],
                'rules'   => $rules
            ]
        ];

        // instantiate a explode-route service with our custom routes array
        $exploder = new Explode($routes);

        // instantiate a parse-path service
        $parser = new Parse($exploder);

        // create a matched route
        $match = (new RouteMatch(['foo_id' => 1]))->setMatchedRouteName('foo');

        // expect a path with a single segment with $rules
        $expected = new Path([new Segment('foo', $rules)]);
        $actual   = $parser->parse($match);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return void
     */
    public function testParseReturnsPathIfRouteIsCollection(): void
    {
        // create two rules arrays (service rules are simpler)
        $foo = ['Path\\To\\Foo\\Rule'];
        $bar = ['Path\\To\\Bar\\Rule'];

        // create a custom routes array
        $routes = [
            'foo' => [
                'type'         => 'segment',
                'options'      => [
                    'route'       => '/foo[/:foo_id]',
                    'constraints' => [
                        'foo_id' => '\d+'
                    ]
                ],
                'rules'        => $foo,
                'child_routes' => [
                    'bar' => [
                        'type'    => 'segment',
                        'options' => [
                            'route'       => '/bar[/:bar_id]',
                            'constraints' => [
                                'bar_id' => '\d+'
                            ]
                        ],
                        'rules'   => $bar
                    ]
                ]
            ]
        ];

        // instantiate a explode-route service with our custom routes array
        $exploder = new Explode($routes);

        // instantiate a parse-path service
        $parser = new Parse($exploder);

        // create a matched route...
        // note that because "bar_id" does not have a value, the request is
        //     considered a "collection" or "edge"
        //
        $params = ['foo_id' => 1];
        $match  = (new RouteMatch($params))->setMatchedRouteName('foo/bar');

        // expect a path with one segment
        $expected = new Path([new Segment('foo', $foo)]);
        $actual   = $parser->parse($match);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return void
     */
    public function testParseReturnsPathIfRouteIsEntity(): void
    {
        // create two rules arrays (service rules are simpler)
        $foo = ['Path\\To\\Foo\\Rule'];
        $bar = ['Path\\To\\Bar\\Rule'];

        // create a custom routes array
        $routes = [
            'foo' => [
                'type'         => 'segment',
                'options'      => [
                    'route'       => '/foo[/:foo_id]',
                    'constraints' => [
                        'foo_id' => '\d+'
                    ]
                ],
                'rules'        => $foo,
                'child_routes' => [
                    'bar' => [
                        'type'    => 'segment',
                        'options' => [
                            'route'       => '/bar[/:bar_id]',
                            'constraints' => [
                                'bar_id' => '\d+'
                            ]
                        ],
                        'rules'   => $bar
                    ]
                ]
            ]
        ];

        // instantiate a explode-route service with our custom routes array
        $exploder = new Explode($routes);

        // instantiate a parse-path service
        $parser = new Parse($exploder);

        // create a matched route...
        // note that because "bar_id" does have a value, the request is considered
        //     to be an "entity" route
        //
        $params = ['foo_id' => 1, 'bar_id' => 1];
        $match  = (new RouteMatch($params))->setMatchedRouteName('foo/bar');

        // expect a path with two segments
        $expected = new Path([new Segment('foo', $foo), new Segment('bar', $bar)]);
        $actual   = $parser->parse($match);

        $this->assertEquals($expected, $actual);
    }
}
