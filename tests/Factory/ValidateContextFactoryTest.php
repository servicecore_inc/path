<?php

namespace ServiceCore\Path\Test\Factory;

use Laminas\ServiceManager\ServiceManager;
use PHPUnit\Framework\TestCase;
use ServiceCore\Path\Context\{Parse as PathParser, Validate as Context};
use ServiceCore\Path\Factory\Validate as Factory;
use ServiceCore\Route\Context\Explode as RouteExploder;

/**
 * @group validate
 * @group validate-factory
 */
class ValidateContextFactoryTest extends TestCase
{
    public function testCreateService(): void
    {
        $services = new ServiceManager();

        // hmm, this is kind of gross, but we need a route-exploder context...
        $routeExploder = new RouteExploder([]);

        // ... to create a path-parser context
        $pathParser = new PathParser($routeExploder);

        // ... to put the path-parser context in the service manager
        $services->setService(PathParser::class, $pathParser);

        $factory = new Factory();

        $this->assertInstanceOf(Context::class, $factory($services, Context::class));
    }
}
