<?php

namespace ServiceCore\Path\Test\Factory;

use Laminas\ServiceManager\ServiceManager;
use PHPUnit\Framework\TestCase;
use ServiceCore\Path\Context\Parse as Context;
use ServiceCore\Path\Factory\Parse as Factory;
use ServiceCore\Route\Context\Explode as RouteExploder;

/**
 * @group parse
 * @group parse-factory
 */
class ParseContextFactoryTest extends TestCase
{
    /**
     * @return void
     */
    public function testCreateService(): void
    {
        $services = new ServiceManager();

        // the path-parser context depends on the route-explode context
        $services->setService(RouteExploder::class, new RouteExploder([]));

        $factory = new Factory();

        $this->assertInstanceOf(Context::class, $factory($services, Context::class));
    }
}
