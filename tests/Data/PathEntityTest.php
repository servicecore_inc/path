<?php

namespace ServiceCore\Path\Test\Data;

use PHPUnit\Framework\TestCase;
use ServiceCore\Path\Data\Path;
use ServiceCore\Path\Data\Segment;

/**
 * @group path
 */
class PathEntityTest extends TestCase
{
    /**
     * @return void
     */
    public function testConstructor(): void
    {
        $segments          = [new Segment('foo', [])];
        $path              = new Path($segments);
        $reflectedPath     = new \ReflectionClass(Path::class);
        $reflectedProperty = $reflectedPath->getProperty('segments');

        $reflectedProperty->setAccessible(true);

        $this->assertSame($segments, $reflectedProperty->getValue($path));
    }

    /**
     * @return void
     */
    public function testGetSegments(): void
    {
        $segments          = [new Segment('foo', [])];
        $path              = new Path([]);
        $reflectedPath     = new \ReflectionClass(Path::class);
        $reflectedProperty = $reflectedPath->getProperty('segments');

        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue($path, $segments);

        $this->assertSame($segments, $path->getSegments());
    }
}
