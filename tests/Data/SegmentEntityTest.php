<?php

namespace ServiceCore\Path\Test\Data;

use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ServiceCore\Path\Data\Segment;

/**
 * @group segment
 */
class SegmentEntityTest extends TestCase
{
    /**
     * @return void
     */
    public function testConstructor(): void
    {
        $name              = 'foo';
        $rules             = ['bar' => 'baz'];
        $segment           = new Segment($name, $rules);
        $reflectedSegment  = new ReflectionClass(Segment::class);
        $reflectedProperty = $reflectedSegment->getProperty('name');

        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue($segment, $name);

        $this->assertEquals($name, $reflectedProperty->getValue($segment));

        $reflectedProperty = $reflectedSegment->getProperty('rules');

        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue($segment, $rules);

        $this->assertEquals($rules, $reflectedProperty->getValue($segment));
    }

    /**
     * @return void
     */
    public function testGetName(): void
    {
        $name              = 'foo';
        $segment           = new Segment('bar', []);
        $reflectedSegment  = new ReflectionClass(Segment::class);
        $reflectedProperty = $reflectedSegment->getProperty('name');

        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue($segment, $name);

        $this->assertEquals($name, $segment->getName());
    }

    /**
     * @return void
     */
    public function testGetRules(): void
    {
        $rules             = ['foo' => 'bar'];
        $segment           = new Segment('foo', []);
        $reflectedSegment  = new ReflectionClass(Segment::class);
        $reflectedProperty = $reflectedSegment->getProperty('rules');

        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue($segment, $rules);

        $this->assertEquals($rules, $segment->getRules());
    }
}
