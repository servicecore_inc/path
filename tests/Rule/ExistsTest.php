<?php

namespace ServiceCore\Path\Test\Rule;

use Doctrine\ORM\EntityManager;
use InvalidArgumentException;
use OutOfBoundsException;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ServiceCore\Path\Data\Segment;
use ServiceCore\Path\Rule\Exists;

/**
 * @group exists
 */
class ExistsTest extends TestCase
{
    /** @var array */
    private $options;

    public function __construct()
    {
        $this->options = [
            'entity'     => Segment::class,
            'constraint' => 'entity_id'
        ];

        parent::__construct();
    }

    public function testConstructThrowsExceptionIfEntityDoesNotExist(): void
    {
        $options = $this->options;

        $this->expectException(InvalidArgumentException::class);

        unset($options['entity']);

        $em = (new ReflectionClass(EntityManager::class))->newInstanceWithoutConstructor();

        new Exists($em, $options);
    }

    public function testConstructThrowsExceptionIfEntityIsNotString(): void
    {
        $options = $this->options;

        $this->expectException(InvalidArgumentException::class);

        $options['entity'] = 1;

        $em = (new ReflectionClass(EntityManager::class))->newInstanceWithoutConstructor();

        new Exists($em, $options);
    }

    public function testConstructThrowsExceptionIfEntityIsNotClass(): void
    {
        $options = $this->options;

        $this->expectException(InvalidArgumentException::class);

        $options['entity'] = 'Path\\To\\Foo';

        $em = (new ReflectionClass(EntityManager::class))->newInstanceWithoutConstructor();

        new Exists($em, $options);
    }

    public function testConstructThrowsExceptionIfConstraintDoesNotExist(): void
    {
        $options = $this->options;

        $this->expectException(InvalidArgumentException::class);

        unset($options['constraint']);

        $em = (new ReflectionClass(EntityManager::class))->newInstanceWithoutConstructor();

        new Exists($em, $options);
    }

    public function testConstructThrowsExceptionIfConstraintIsNotString(): void
    {
        $options = $this->options;

        $this->expectException(InvalidArgumentException::class);

        $options['constraint'] = 1;

        $em = (new ReflectionClass(EntityManager::class))->newInstanceWithoutConstructor();

        new Exists($em, $options);
    }

    public function testGetOptionThrowsExceptionIfOptionDoesNotExist(): void
    {
        $options = $this->options;

        $this->expectException(OutOfBoundsException::class);

        $em = (new ReflectionClass(EntityManager::class))->newInstanceWithoutConstructor();

        $rule = new Exists($em, $options);

        $rule->getOption('foo');
    }

    public function testGetOptions(): void
    {
        $em                = (new ReflectionClass(EntityManager::class))->newInstanceWithoutConstructor();
        $rule              = new Exists($em, $this->options);
        $reflectedRule     = new ReflectionClass(Exists::class);
        $reflectedProperty = $reflectedRule->getProperty('options');

        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue($rule, $this->options);

        $this->assertEquals($this->options, $rule->getOptions());
    }

    public function testGetEntityManager(): void
    {
        $em                = (new ReflectionClass(EntityManager::class))->newInstanceWithoutConstructor();
        $rule              = new Exists($em, $this->options);
        $reflectedRule     = new ReflectionClass(Exists::class);
        $reflectedProperty = $reflectedRule->getProperty('entityManager');

        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue($rule, $em);

        $this->assertSame($em, $rule->getEntityManager());
    }

    public function testGetOptionReturnsValueIfOptionDoesExist(): void
    {
        $em   = (new ReflectionClass(EntityManager::class))->newInstanceWithoutConstructor();
        $rule = new Exists($em, $this->options);

        $this->assertEquals($this->options['entity'], $rule->getOption('entity'));
    }

    public function testHasOptionReturnsTrueIfOptionDoesExist(): void
    {
        $em   = (new ReflectionClass(EntityManager::class))->newInstanceWithoutConstructor();
        $rule = new Exists($em, $this->options);

        $this->assertTrue($rule->hasOption('entity'));
    }

    public function testHasOptionReturnsFalseIfOptionDoesNotExist(): void
    {
        $em   = (new ReflectionClass(EntityManager::class))->newInstanceWithoutConstructor();
        $rule = new Exists($em, $this->options);

        $this->assertFalse($rule->hasOption('foo'));
    }
}
