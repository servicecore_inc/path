<?php

namespace ServiceCore\Path\Test\Rule;

use Doctrine\ORM\EntityManager;
use InvalidArgumentException;
use OutOfBoundsException;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ServiceCore\Path\Data\Segment;
use ServiceCore\Path\Rule\BelongsTo;

/**
 * @group belongs
 */
class BelongsToTest extends TestCase
{
    /** @var array */
    private $options;

    public function __construct()
    {
        $this->options = [
            'child'  => [
                'entity'     => Segment::class,
                'constraint' => 'entity_id',
                'method'     => 'getName'
            ],
            'parent' => [
                'constraint' => 'parent_entity_id'
            ]
        ];

        parent::__construct();
    }

    public function testConstructThrowsExceptionChildOptionsDoNotExist(): void
    {
        $this->expectException(InvalidArgumentException::class);

        unset($this->options['child']);

        $em = new ReflectionClass(EntityManager::class);

        new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);
    }

    public function testConstructThrowsExceptionIfChildOptionsIsNotArray(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $this->options['child'] = 1;

        $em = new ReflectionClass(EntityManager::class);

        new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);
    }

    public function testConstructThrowsExceptionIfChildEntityDoesNotExist(): void
    {
        $this->expectException(InvalidArgumentException::class);

        unset($this->options['child']['entity']);

        $em = new ReflectionClass(EntityManager::class);

        new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);
    }

    public function testConstructThrowsExceptionIfChildEntityIsNotAString(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $this->options['child']['entity'] = 1;

        $em = new ReflectionClass(EntityManager::class);

        new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);
    }

    public function testConstructThrowsExceptionIfChildEntityItNotClass(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $this->options['child']['entity'] = 'foo';

        $em = new ReflectionClass(EntityManager::class);

        new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);
    }

    public function testConstructThrowsExceptionIfChildConstraintDoesNotExist(): void
    {
        $this->expectException(InvalidArgumentException::class);

        unset($this->options['child']['constraint']);

        $em = new ReflectionClass(EntityManager::class);

        new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);
    }

    public function testConstructThrowsExceptionIfChildConstraintIsNotString(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $this->options['child']['constraint'] = 1;

        $em = new ReflectionClass(EntityManager::class);

        new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);
    }

    public function testConstructThrowsExceptionIfChildMethodDoesNotExist(): void
    {
        $this->expectException(InvalidArgumentException::class);

        unset($this->options['child']['method']);

        $em = new ReflectionClass(EntityManager::class);

        new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);
    }

    public function testConstructThrowsExceptionIfChildMethodIsNotString(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $this->options['child']['method'] = 1;

        $em = new ReflectionClass(EntityManager::class);

        new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);
    }

    public function testConstructThrowsExceptionIfChildMethodIsNotMethod(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $this->options['child']['method'] = 'getFoo';

        $em = new ReflectionClass(EntityManager::class);

        new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);
    }

    public function testConstructThrowsExceptionIfParentOptionsDoNotExist(): void
    {
        $this->expectException(InvalidArgumentException::class);

        unset($this->options['parent']);

        $em = new ReflectionClass(EntityManager::class);

        new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);
    }

    public function testConstructThrowsExceptionIfParentConstraintDoesNotExist(): void
    {
        $this->expectException(InvalidArgumentException::class);

        unset($this->options['parent']['constraint']);

        $em = new ReflectionClass(EntityManager::class);

        new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);
    }

    public function testConstructThrowsExceptionIfParentConstraintIsNotString(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $this->options['parent']['constraint'] = 1;

        $em = new ReflectionClass(EntityManager::class);

        new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);
    }

    public function testGetOptionThrowsExceptionIfOptionDoesNotExist(): void
    {
        $this->expectException(OutOfBoundsException::class);

        $em   = new ReflectionClass(EntityManager::class);
        $rule = new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);

        $rule->getOption('foo');
    }

    public function testGetOptions(): void
    {
        $em                = new ReflectionClass(EntityManager::class);
        $rule              = new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);
        $reflectedRule     = new ReflectionClass(BelongsTo::class);
        $reflectedProperty = $reflectedRule->getProperty('options');

        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue($rule, $this->options);

        $this->assertEquals($this->options, $rule->getOptions());
    }

    public function testGetOptionReturnsValueIfOptionDoesExist(): void
    {
        $em   = new ReflectionClass(EntityManager::class);
        $rule = new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);

        $this->assertEquals($this->options['child'], $rule->getOption('child'));
    }

    public function testHasOptionReturnsTrueIfOptionDoesExist(): void
    {
        $em   = new ReflectionClass(EntityManager::class);
        $rule = new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);

        $this->assertTrue($rule->hasOption('child'));
    }

    public function testHasOptionReturnsFalseIfOptionDoesNotExist(): void
    {
        $em   = new ReflectionClass(EntityManager::class);
        $rule = new BelongsTo($em->newInstanceWithoutConstructor(), $this->options);

        $this->assertFalse($rule->hasOption('foo'));
    }

    public function testGetEntityManager(): void
    {
        $em                = new ReflectionClass(EntityManager::class);
        $em                = $em->newInstanceWithoutConstructor();
        $rule              = new BelongsTo($em, $this->options);
        $reflectedRule     = new ReflectionClass(BelongsTo::class);
        $reflectedProperty = $reflectedRule->getProperty('entityManager');

        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue($rule, $em);

        $this->assertSame($em, $rule->getEntityManager());
    }
}
