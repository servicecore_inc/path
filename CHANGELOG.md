# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 2.0.3

### Added

- CHANGELOG.md
- PHP 8.1/8.2 support
