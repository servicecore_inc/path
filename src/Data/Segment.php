<?php

namespace ServiceCore\Path\Data;

/**
 * A path segment
 *
 * A path segment has a name and a set of rules.
 */
class Segment
{
    /** @var string */
    private $name;

    /** @var array */
    private $rules;

    /**
     * @param string $name
     * @param array  $rules
     */
    public function __construct(string $name, array $rules)
    {
        $this->name  = $name;
        $this->rules = $rules;
    }

    /**
     * @return  string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return  array
     */
    public function getRules(): array
    {
        return $this->rules;
    }
}
