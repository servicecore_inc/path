<?php

namespace ServiceCore\Path\Data;

/**
 * A path
 *
 * A path is composed of path segments.
 */
class Path
{
    /** @var array */
    private $segments;

    /**
     * @param array $segments
     */
    public function __construct(array $segments)
    {
        $this->segments = $segments;
    }

    /**
     * @return  array
     */
    public function getSegments(): array
    {
        return $this->segments;
    }
}
