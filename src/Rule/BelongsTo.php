<?php

namespace ServiceCore\Path\Rule;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use InvalidArgumentException;

/**
 * The belongs-to rule
 *
 * I'll return true if a entity belongs-to another. Keep in mind, I assume the
 * child is able to get it's parent (e.g., User::getCompany()).
 *
 * I accept two options "child", an array of child-entity options; and "parent", an
 * array of parent-entity options.
 *
 * The "child" options requires three sub-options: "entity", the child-entity's
 * fully-qualified class name (with leading "\"); "constraint", the child-entity's
 * route constraint name; and, "method", the name of the method that return's the
 * child's parent-entity.
 *
 * The "parent" option requies one sub-option: "constraint", the parent-entity's
 * route constraint name.
 *
 * For example, where bar belongs to foo:
 *     [
 *         'name'    => '\\Path\\To\\BelongsTo',
 *         'options' => [
 *             'child' => [
 *                 'entity'     => '\\Path\\To\\Bar',
 *                 'constraint' => 'bar_id',
 *                 'method'     => 'getFoo'
 *             ],
 *             'parent' => [
 *                 'constraint' => 'foo_id'
 *             ]
 *         ]
 *     ]
 */
class BelongsTo extends Rule
{
    public function __construct(EntityManagerInterface $entityManager, array $options)
    {
        $this->validate($options);

        parent::__construct($entityManager, $options);
    }

    /**
     * I'll return true if the child- and parent-constraints exist as route
     * parameters and the child-entity exists and the value of the child's parent id
     * matches the value of the parent's constraint in the route.
     *
     * @param   array $parameters the route's parameters, indexed by constraint
     * @return  bool
     */
    public function apply(array $parameters): bool
    {
        // get the parent and child options arrays
        $childOptions  = $this->getOption('child');
        $parentOptions = $this->getOption('parent');

        // if the *child* constraint exists as a parameter
        // and if the *parent* constraint exists as a parameter
        if (\array_key_exists($childOptions['constraint'], $parameters)
            && \array_key_exists($parentOptions['constraint'], $parameters)
        ) {
            // if the child-entity exists
            /** @var EntityManager $entityManger */
            $entityManger = $this->getEntityManager();

            /** @var EntityRepository $repository */
            $repository = $entityManger->getRepository($childOptions['entity']);
            $child      = $repository->find($parameters[$childOptions['constraint']]);

            if ($child) {
                // if the parent-entity exists
                $getter = $childOptions['method'];

                if (null !== ($parent = $child->$getter())) {
                    // if the ids match...
                    // keep in mind, use loose comparison here, because Doctrine
                    //     will return an int and the route parameter is likely
                    //     a string
                    //
                    $savedId = $parent->getId();
                    $routeId = $parameters[$parentOptions['constraint']];
                    if ($savedId == $routeId) {
                        // great success!

                        return true;
                    }
                } elseif ($this->hasOption('orNull')) {
                    $orNull = $this->getOption('orNull');

                    if ($orNull && $orNull === true) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private function validate(array $options): void
    {
        // validate the "child" options
        $this->validateChild($options);

        // validate the "parent" options
        $this->validateParent($options);
    }

    private function validateChild(array $options): void
    {
        // if a "child" option does not exist, short-circuit
        if (!\array_key_exists('child', $options)) {
            throw new InvalidArgumentException(
                "The 'belongs-to' rule expects a 'child' option array"
            );
        }

        // if the "child" option is not an array, short-circuit
        if (!\is_array($options['child'])) {
            throw new InvalidArgumentException(
                "The 'belongs-to' rule expects the 'child' option to be an array"
            );
        }

        // otherwise, get the child options
        $options = $options['child'];

        // validate the child's "entity", "constraint", and "method" options
        $this->validateChildEntity($options);
        $this->validateChildConstraint($options);
        $this->validateChildMethod($options);
    }

    private function validateChildEntity(array $options): void
    {
        // if "entity" does not exist, short-circuit
        if (!\array_key_exists('entity', $options)) {
            throw new InvalidArgumentException(
                "The 'belongs-to' rule expects a 'child.entity' option"
            );
        }

        // if the "entity" is not a string, short-circuit
        if (!\is_string($options['entity'])) {
            throw new InvalidArgumentException(
                "The 'belongs-to' rule expects the 'child.entity' option to be a "
                . 'string'
            );
        }

        // if the "entity" is not a valid class name, short-circuit
        if (!\class_exists($options['entity'])) {
            throw new InvalidArgumentException(
                "The 'belongs-to' rule expects the 'child.entity' option to be a "
                . "valid class name; class {$options['entity']} could not be "
                . 'found'
            );
        }
    }

    private function validateChildConstraint(array $options): void
    {
        // if "constraint" does not exist, short-circuit
        if (!\array_key_exists('constraint', $options)) {
            throw new InvalidArgumentException(
                "The 'belongs-to' rule expects a 'child.constraint' option"
            );
        }

        // if "constraint" is not a string, short-circuit
        if (!\is_string($options['constraint'])) {
            throw new InvalidArgumentException(
                "The 'belongs-to' rule expects the 'child.constraint' option to be "
                . 'a string'
            );
        }
    }

    private function validateChildMethod(array $options): void
    {
        // if "method" does not exist, short-circuit
        if (!\array_key_exists('method', $options)) {
            throw new InvalidArgumentException(
                "The 'belongs-to' rule expects a 'child.method' option"
            );
        }

        // if "method" is not a string, short-circuit
        if (!\is_string($options['method'])) {
            throw new InvalidArgumentException(
                "The 'belongs-to' rule expects the 'child.method' option to be a "
                . 'string'
            );
        }

        // if "method" is not actually a method, short-circuit
        if (!\method_exists($options['entity'], $options['method'])) {
            throw new InvalidArgumentException(
                "The 'belongs-to' rule expects the 'child.method' option to be a "
                . 'valid method of the child-entity'
            );
        }
    }

    private function validateParent(array $options): void
    {
        // if "parent" does not exist, short-circuit
        if (!\array_key_exists('parent', $options)) {
            throw new InvalidArgumentException(
                "The 'belongs-to' rule expects a 'parent' option'"
            );
        }

        // if "parent" is not an array, short-circuit
        if (!\is_array($options['parent'])) {
            throw new InvalidArgumentException(
                "The 'belongs-to' rule expects the 'parent' option to be an array"
            );
        }

        // otherwise, get the options
        $options = $options['parent'];

        // validate the parent constraint
        $this->validateParentConstraint($options);
    }

    public function validateParentConstraint(array $options): void
    {
        // if "constraint" does not exist, short-circuit
        if (!\array_key_exists('constraint', $options)) {
            throw new InvalidArgumentException(
                "The 'belongs-to' rule expects a 'parent.constraint' option"
            );
        }

        // if the "constraint" is not a string, short-circuit
        if (!\is_string($options['constraint'])) {
            throw new InvalidArgumentException(
                "The 'belongs-to' rule expects the 'parent.constraint' to be a "
                . 'string'
            );
        }
    }
}
