<?php

namespace ServiceCore\Path\Rule;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use InvalidArgumentException;

/**
 * The exists rule
 *
 * I'll return true if an entity exists in the database.
 *
 * I accept two options: "entity", a fully-qualified class name to a Doctrine entity;
 * and "constraint",the name of the constraint in the route's parameters that
 * corresponds to the entity's id.
 *
 * For example:
 *
 *     [
 *         'name'    => 'Path\To\Exists',
 *         'options' => [
 *             'entity'     => 'Path\To\Foo',
 *             'constraint' => 'foo_id'
 *         ]
 *     ]
 *
 * @todo It might be nice to add a third, optional option, "method", which if
 *       defined will use the entity's repository instead of Doctrine's generic find()
 *       method; it would allow entities to be found by other parameters instead of
 *       their id
 */
class Exists extends Rule
{
    public function __construct(EntityManagerInterface $entityManager, array $options)
    {
        // validate the "entity" option
        $this->validateEntity($options);

        // validate the "constraint" option
        $this->validateConstraint($options);

        // otherwise, make it so No 1!
        parent::__construct($entityManager, $options);
    }

    public function apply(array $parameters): bool
    {
        // if the constraint doesn't exist in the parameters, short-circuit
        if (!\array_key_exists($this->getOption('constraint'), $parameters)) {
            return false;
        }

        // otherwise, get the entity's id
        $id = $parameters[$this->getOption('constraint')];

        // determine whether or not the entity exists
        /** @var EntityManager $entityManger */
        $entityManger = $this->getEntityManager();

        /** @var EntityRepository $repository */
        $repository = $entityManger->getRepository($this->getOption('entity'));

        try {
            $exists = (bool)$repository->find($id);
        } catch (NoResultException $e) {
            $exists = false;
        }

        return $exists;
    }

    private function validateConstraint(array $options): void
    {
        // if the "constraint" option does not exist, short-circuit
        if (!\array_key_exists('constraint', $options)) {
            throw new InvalidArgumentException(
                "The 'exists' rule expects a 'constraint' option"
            );
        }

        // if the "constraint" option is not a string, short-circuit
        if (!\is_string($options['constraint'])) {
            throw new InvalidArgumentException(
                "The 'exists' rule expects the 'constraint' option to be a string"
            );
        }
    }

    private function validateEntity(array $options): void
    {
        // if an "entity" option does not exist, short-circuit
        if (!\array_key_exists('entity', $options)) {
            throw new InvalidArgumentException(
                "The 'exists' rule expects an 'entity' option"
            );
        }

        // if the "entity" option is not a string, short-circuit
        if (!\is_string($options['entity'])) {
            throw new InvalidArgumentException(
                "The 'exists' rule expects the 'entity' option to be a string"
            );
        }

        // if the "entity" option's class does not exist, short-circuit
        if (!\class_exists($options['entity'])) {
            throw new InvalidArgumentException(
                "The 'exists' rule expects the 'entity' option to be a fully-"
                    . "qualified class name; class '{$options['entity']}' does not "
                    . 'exist'
            );
        }
    }
}
