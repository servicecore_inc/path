<?php

namespace ServiceCore\Path\Rule;

use Doctrine\ORM\EntityManagerInterface;
use OutOfBoundsException;

/**
 * A newable rule
 */
abstract class Rule implements RuleInterface
{
    /** @var array */
    protected $options = [];

    /** @var EntityManagerInterface */
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager, array $options = [])
    {
        $this->entityManager = $entityManager;
        $this->options       = $options;
    }

    abstract public function apply(array $parameters): bool;

    public function getOptions(): array
    {
        return $this->options;
    }

    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @param string $name
     *
     * @return mixed
     * @throws OutOfBoundsException
     */
    public function getOption(string $name)
    {
        if (!\array_key_exists($name, $this->options)) {
            throw new OutOfBoundsException(
                "Option {$name} does not exist"
            );
        }

        return $this->options[$name];
    }

    public function hasOption(string $name): bool
    {
        return \array_key_exists($name, $this->options);
    }
}
