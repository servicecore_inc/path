<?php

namespace ServiceCore\Path\Rule;

/**
 * The rule interface
 *
 * The rule interface must be implemented by newable rules and service rules.
 */
interface RuleInterface
{
    /**
     * Returns true if the rule passes
     *
     * @param   mixed[]  the route parameters, indexed by constraint name
     * @return  bool
     */
    public function apply(array $parameters): bool;
}
