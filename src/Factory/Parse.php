<?php

namespace ServiceCore\Path\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Path\Context\Parse as PathParser;
use ServiceCore\Route\Context\Explode as RouteExploder;

/**
 * The factory for the parse-path context
 */
class Parse implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return PathParser
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): PathParser
    {
        return new PathParser($container->get(RouteExploder::class));
    }
}
