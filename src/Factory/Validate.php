<?php

namespace ServiceCore\Path\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Path\Context\Validate as PathValidator;

/**
 * The factory for the validate-path context
 */
class Validate implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return PathValidator
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): PathValidator
    {
        return new PathValidator($container);
    }
}
