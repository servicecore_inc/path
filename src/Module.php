<?php

namespace ServiceCore\Path;

use Laminas\Http\Request;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\Mvc\MvcEvent as Event;
use ServiceCore\ApiProblem\NotFound;
use ServiceCore\Path\Context\Parse;
use ServiceCore\Path\Context\Validate;

class Module implements ConfigProviderInterface
{
    /**
     * @var  int  the priority for the module's EVENT_ROUTE listener; keep in mind,
     *     priority matters! this value MUST be less than the route match event (-1),
     *     and greater than the Authorization module's listener (-30); otherwise, in
     *     the former case, an exception will the thrown if the route doesn't exist,
     *     and in the latter case, at the very least, we waste resources authorizing
     *     a route that may not be not valid
     */
    private const PRIORITY = -20;

    public function getConfig(): array
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * I'll listen for the application's EVENT_ROUTE. If the route is invalid, I'll
     * respond 404. Otherwise, I'll allow the request to continue.
     *
     * @param   Event $event the application's mvc-event
     * @return  void
     */
    public function onBootstrap(Event $event): void
    {
        // if the request is an http request, not a console request
        if ($event->getRequest() instanceof Request) {
            // register the event listener
            $event->getApplication()->getEventManager()->attach(
                Event::EVENT_ROUTE,
                function (Event $event) {
                    $sm = $event->getApplication()->getServiceManager();

                    /** @var Parse $parse */
                    $parse = $sm->get(Parse::class);
                    $path  = $parse->parse($event->getRouteMatch());

                    /** @var Validate $validate */
                    $validate = $sm->get(Validate::class);
                    $isValid  = $validate->validate($path, $event->getRouteMatch()->getParams());

                    if (!$isValid) {
                        return new NotFound();
                    }

                    return;
                },
                self::PRIORITY
            );
        }
    }
}
