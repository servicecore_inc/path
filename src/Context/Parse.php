<?php

namespace ServiceCore\Path\Context;

use Laminas\Router\Http\RouteMatch;
use OutOfBoundsException;
use ServiceCore\Path\Data\Path;
use ServiceCore\Path\Data\Segment;
use ServiceCore\Route\Context\Explode as Exploder;

/**
 * The parse-path context
 *
 * I'll parse a *route* (i.e., a Zend Framework Matched Route) into a *path* (i.e., a
 * sequence of segments with rules).
 */
class Parse
{
    /** @var Exploder */
    private $exploder;

    public function __construct(Exploder $exploder)
    {
        $this->exploder = $exploder;
    }

    public function parse(RouteMatch $match): Path
    {
        $newSegments = [];

        // get the matched route name
        $name = $match->getMatchedRouteName();

        // explode the route into its configuration segments
        $route = $this->exploder->explode($name);

        // loop through the route's segments
        foreach ($route->getSegments() as $oldSegment) {
            // if the "old" segment is missing a rules key, short-circuit
            if (!\array_key_exists('rules', $oldSegment->getConfiguration())) {
                throw new OutOfBoundsException(
                    "The '{$oldSegment->getName()}' route segment is missing a "
                    . "'rules' configuration key; it may be an empty array, but "
                    . 'it must exist'
                );
            }

            // otherwise, get the "old" segment's rules
            $rules = $oldSegment->getConfiguration()['rules'];

            // create and append a "new" path segment
            $newSegments[] = new Segment($oldSegment->getName(), $rules);
        }

        // if "new" segments exist (i.e., the route was not empty)
        if ($newSegments) {
            // at this point, we have path segments for each route segment; however,
            //     if the route is a "collection" or an "edge", the last segment's
            //     rules should not apply (dear god, please let this assumption be
            //     correct); as a result, we need to determine if the route is a
            //     "collection"
            //

            // get the last route segment's configuration
            $configuration = $route->getLast()->getConfiguration();

            // if the last segment doesn't have a constraint or the constraint
            //     doesn't have a value in the parameters array, the route is a
            //     "collection" or "edge"
            // pop the last segment off the end of the array
            //
            if (null === ($constraint = $this->getConstraint($configuration))
                || !\array_key_exists($constraint, $match->getParams())
            ) {
                if ($this->isCollection($configuration)) {
                    \array_pop($newSegments);
                }
            }
        }

        return new Path($newSegments);
    }

    /**
     * I expect exactly one constraint to have the suffix "_id".
     * Otherwise, I cannot determine the constraint name, and I'll return null.
     *
     * @param array $configuration
     *
     * @return null|string
     */
    private function getConstraint(array $configuration): ?string
    {
        $constraint = null;

        // if route segment options exist and constraints exist in options
        if (\array_key_exists('options', $configuration)
            && \array_key_exists('constraints', $configuration['options'])
        ) {
                // get the constraint names
                $constraints = \array_keys(
                    $configuration['options']['constraints']
                );
                // remove anything without the "_id" prefix
                $constraints = \array_filter(
                    $constraints,
                    function ($v) {
                        return \substr($v, -3) === '_id';
                    }
                );
                // if exactly one constraint remains
                if (\count($constraints) === 1) {
                    $constraint = \reset($constraints);
                }

        }

        return $constraint;
    }

    private function isCollection(array $configuration): bool
    {
        if (\array_key_exists('options', $configuration)) {
            // if the isCollection option is not present, assume it is a collection by default
            if (!\array_key_exists('isCollection', $configuration['options'])) {
                return true;
            }

            if ($configuration['options']['isCollection'] === true) {
                return true;
            }
        }

        return false;
    }
}
