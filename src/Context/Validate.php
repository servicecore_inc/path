<?php

namespace ServiceCore\Path\Context;

use Closure;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use InvalidArgumentException;
use ServiceCore\Path\Data\Path;
use ServiceCore\Path\Data\Segment;
use ServiceCore\Path\Rule\RuleInterface;

class Validate
{
    /** @var ContainerInterface */
    private $services;

    public function __construct(ContainerInterface $services)
    {
        $this->services = $services;
    }

    public function validate(Path $path, array $parameters): bool
    {
        $skippedSegments = $parameters['skippedSegments'] ?? null;

        // loop through the path's segments
        /** @var Segment $segment */
        foreach ($path->getSegments() as $segment) {
            if ($skippedSegments && \in_array($segment->getName(), $skippedSegments, true)) {
                continue;
            }

            // loop through the segment's rules
            foreach ($segment->getRules() as $rule) {
                // if the rule fails, short-circuit
                if (!$this->apply($rule, $parameters)) {
                    // nohoho, you fail!
                    return false;
                }
            }
        }

        return true;
    }

    private function apply($rule, array $parameters): bool
    {
        // apply the newable, service, or closure rule
        if (\is_array($rule)) {
            return $this->applyNewable($rule, $parameters);
        }

        if (\is_string($rule)) {
            return $this->applyService($rule, $parameters);
        }

        if (\is_callable($rule)) {
            return $this->applyClosure($rule, $parameters);
        }

        throw new InvalidArgumentException(
            'A rule must be an array, string, or closure; ' . \gettype($rule)
            . ' given'
        );
    }

    private function applyClosure(Closure $closure, array $parameters): bool
    {
        $em = $this->services->get(EntityManager::class);

        return $closure->call($em, $parameters);
    }

    private function applyNewable(array $data, array $parameters): bool
    {
        // if the "name" key does not exist, short-circuit
        if (!\array_key_exists('name', $data)) {
            throw new InvalidArgumentException(
                "A newable rule must be an array with a 'name' key"
            );
        }

        // if options exist and are not an array, short-circuit
        if (\array_key_exists('options', $data) && !\is_array($data['options'])) {
            throw new InvalidArgumentException(
                "If a newable rule has an 'options' key, it must be an array"
            );
        }

        // if the class does not exist, short-circuit
        if (!\class_exists($data['name'])) {
            throw new InvalidArgumentException(
                "The newable rule '{$data['name']}' does not exist"
            );
        }

        $em = $this->services->get(EntityManager::class);

        // otherwise, get the rule
        $rule = new $data['name']($em, $data['options'] ?? []);

        // if the rule does not implement the rule interface, short-circuit
        if (!$rule instanceof RuleInterface) {
            throw new InvalidArgumentException(
                "The newable rule '{$data['name']}' must implement the rule "
                . 'interface'
            );
        }

        return $rule->apply($parameters);
    }

    private function applyService(string $name, array $parameters): bool
    {
        // if the service does not exist, short-circuit
        if (!$this->services->has($name)) {
            throw new InvalidArgumentException(
                "The service '{$name}' does not exist in the service manager"
            );
        }

        // if the service does not implement the rule interface, short-circuit
        if (!$this->services->get($name) instanceof RuleInterface) {
            throw new InvalidArgumentException(
                "The service '{$name}' must implement the rule interface"
            );
        }

        // otherwise, apply the rule
        return $this->services->get($name)->apply($parameters);
    }
}
